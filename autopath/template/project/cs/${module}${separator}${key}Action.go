package @{packagePath}

import (
	"cs/utils"
	"cs/utils/base"
	"github.com/gogf/gf/g"
	"github.com/gogf/gf/g/net/ghttp"
	"github.com/gogf/gf/g/os/glog"
	"github.com/gogf/gf/g/os/gtime"
	"github.com/gogf/gf/g/util/gconv"
)

type @{strutils.toUpperCaseFirst(crud.urlKey)}Action struct {
	base.BaseRouter
}

var (
	actionName@{strutils.toUpperCaseFirst(crud.urlKey)} = "@{strutils.toUpperCaseFirst(crud.urlKey)}Action"
)

// path: /index
func (action *@{strutils.toUpperCaseFirst(crud.urlKey)}Action) Index(r *ghttp.Request) {
	tplFile := "pages/@{packagePath}/@{crud.urlKey}_index.html"
	err := r.Response.WriteTpl(tplFile, g.Map{
		"now": gtime.Datetime(),
	})

	if err != nil {
		glog.Error(err)
	}
}

// path: /get/{id}
func (action *@{strutils.toUpperCaseFirst(crud.urlKey)}Action) Get(r *ghttp.Request) {
	id := r.GetInt("id")
	model := @{crud.table.className}{Id: id}.Get()
	if model.Id <= 0 {
		base.Fail(r, actionName@{strutils.toUpperCaseFirst(crud.urlKey)}+" get fail")
	}

	base.Succ(r, model)
}

// path: /delete/{id}
func (action *@{strutils.toUpperCaseFirst(crud.urlKey)}Action) Delete(r *ghttp.Request) {
	id := r.GetInt("id")

	model := @{crud.table.className}{Id: id}
	model.UpdateId = base.GetUser(r).Id
	model.UpdateTime = utils.GetNow()

	num := model.Delete()
	if num <= 0 {
		base.Fail(r, actionName@{strutils.toUpperCaseFirst(crud.urlKey)}+" delete fail")
	}

	base.Succ(r, "")
}

// path: /save
func (action *@{strutils.toUpperCaseFirst(crud.urlKey)}Action) Save(r *ghttp.Request) {
	model := @{crud.table.className}{}
	err := gconv.Struct(r.GetPostMap(), &model)
	if err != nil {
		glog.Error(actionName@{strutils.toUpperCaseFirst(crud.urlKey)}+" save struct error", err)
		base.Error(r, "save error")
	}

	userId := base.GetUser(r).Id

	model.UpdateId = userId
	model.UpdateTime = utils.GetNow()

	var num int64
	if model.Id <= 0 {
		model.CreateId = userId
		model.CreateTime = utils.GetNow()
		num = model.Insert()
	} else {
		num = model.Update()
	}

	if num <= 0 {
	    base.Fail(r, actionName@{strutils.toUpperCaseFirst(crud.urlKey)}+" save fail")
	}

	base.Succ(r, "")
}

// path: /list
func (action *@{strutils.toUpperCaseFirst(crud.urlKey)}Action) List(r *ghttp.Request) {
    form := base.NewForm(r.GetPostMap())
	model := @{crud.table.className}{}

	list := model.List(&form)
	base.Succ(r, list)
}

// path: /page
func (action *@{strutils.toUpperCaseFirst(crud.urlKey)}Action) Page(r *ghttp.Request) {
	form := base.NewForm(r.GetPostMap())
	model := @{crud.table.className}{}

	page := model.Page(&form)
	base.Succ(r, g.Map{"list": page, "form": form})
}

// path: /jqgrid
func (action *@{strutils.toUpperCaseFirst(crud.urlKey)}Action) Jqgrid(r *ghttp.Request) {
	form := base.NewForm(r.GetPostMap())
	model := @{crud.table.className}{}

	page := model.Page(&form)
	r.Response.WriteJson(g.Map{
		"page":    form.Page,
		"rows":    page,
		"total":   form.TotalPage,
		"records": form.TotalSize,
	})
}