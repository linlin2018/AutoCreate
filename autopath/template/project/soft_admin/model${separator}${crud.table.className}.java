package @{packagePath}.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.channelsoft.datacenter.component.base.BaseModel;

import java.io.Serializable;

@TableName(value = "@{crud.table.tableName}")
public class @{crud.table.className} extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1L;

    // columns START
	# for(column in crud.table.columns){ #
		# if(column.columnName=='enable'||column.columnName=='update_time'||column.columnName=='update_id'||column.columnName=='create_time'||column.columnName=='create_id'){ #
		# 	continue; #
		# } #

		# if(column.pk) { #
	@TableId(value = "@{strutils.toLowerCase(column.columnName)}")
		# } else { #
			# if(strutils.toLowerCase(column.columnName)!=strutils.toLowerCaseFirst(column.columnJavaName) ) {#
	@TableField(value = "@{strutils.toLowerCase(column.columnName)}")
			# } #
		# } #
	private @{column.javaType} @{strutils.toLowerCaseFirst(column.columnJavaName)};  // @{column.remarks}
    # } #
	// columns END

	# for(column in crud.table.columns){ #
	# if(column.pk) { #
	public Serializable pkVal() {
		return @{strutils.toLowerCase(column.columnName)};
	}
	# } #
	# } #

    # for(column in crud.table.columns){ #
		# if(column.columnName=='enable'||column.columnName=='update_time'||column.columnName=='update_id'||column.columnName=='create_time'||column.columnName=='create_id'){ #
		# 	continue; #
		# } #

	public @{column.javaType} get@{strutils.toUpperCaseFirst(column.columnJavaName)}() {
		return @{strutils.toLowerCaseFirst(column.columnJavaName)};
	}

    public void set@{strutils.toUpperCaseFirst(column.columnJavaName)}(@{column.javaType} @{strutils.toLowerCaseFirst(column.columnJavaName)}) {
    	this.@{strutils.toLowerCaseFirst(column.columnJavaName)} = @{strutils.toLowerCaseFirst(column.columnJavaName)};
    }
	# } #
	
	@Override
	public String toString() {
		String log = ""; 
	# for(column in crud.table.columns){ #
		# if(column.columnName=='enable'||column.columnName=='update_time'||column.columnName=='update_id'||column.columnName=='create_time'||column.columnName=='create_id'){ #
		# 	continue; #
		# } #
		log += "[@{strutils.toLowerCaseFirst(column.columnJavaName)}:" + get@{strutils.toUpperCaseFirst(column.columnJavaName)}() + "]";
	# } #
		log += super.toString();
		return log;
	}
}