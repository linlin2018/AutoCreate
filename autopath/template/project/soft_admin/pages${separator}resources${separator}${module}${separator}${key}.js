var vm = new Vue({
    el:'\#data_wrapper',
    data:{
        showList: true,
        showEdit: false,
        showView: false,
        title: null,
        model:{
        # for(entry in crud.listMap){ #
        @{entry.value.javaKey}:null, // @{entry.value.name}
        # } #
        id:null
        }
},
methods: {
    view: function(id){
        var id = id || null;
        if(id == null){
            Alert('请选择修改数据');
            return ;
        }

        var url = dudu.ctx + "/@{module}/@{crud.urlKey}/get/" + id;
        dudu.get(url, function(result){
            vm.showList = false;
            vm.showEdit = false;
            vm.showView = true;

            vm.title = "查看@{crud.name}";
            vm.model = result.data;
        });
    },
    add: function(){
        vm.showList = false;
        vm.showEdit = true;
        vm.showView = false;

        vm.title = "新增@{crud.name}";
        vm.model = {
            # for(entry in crud.listMap){ #
            @{entry.value.javaKey}:null, // @{entry.value.name}
            # } #
            id:null
        };
    },
    update: function (id) {
        var id = id || null;
        if(id == null){
            Alert('请选择修改数据');
            return ;
        }

        var url = dudu.ctx + "/@{module}/@{crud.urlKey}/get/" + id;
        dudu.get(url, function(result){
            vm.showList = false;
            vm.showEdit = true;
            vm.showView = false;

            vm.title = "修改@{crud.name}";
            vm.model = result.data;
        });
    },
    del: function (id) {
        var id = id || null;
        if(id == null){
            Alert('请选择删除数据');
            return ;
        }

        Confirm('确定要删除选中的记录？', function(){
            var url = dudu.ctx + "/@{module}/@{crud.urlKey}/delete/" + id;
            dudu.post(url, null, function (result) {
                if(result.code === 0){
                    Alert('操作成功', function(index){
                        vm.reload();
                    });
                }else{
                    Alert(result.msg);
                }
            });
        });
    },
    save: function () {
        if (!validForm()) {
            return;
        }

        var url = dudu.ctx + "/@{module}/@{crud.urlKey}/save";
        dudu.post(url, vm.model, function (result) {
            if(result.code === 0){
                Alert('操作成功', function(index){
                    vm.reload();
                });
            }else{
                Alert(result.msg);
            }
        });
    },
    reload: function () {
        vm.showList = true;
        vm.showEdit = false;
        vm.showView = false;

        var fields = $("\#showList form").serializeArray();
        var jsonData = {};
        jQuery.each( fields, function(i, field){
            jsonData[field.name]=field.value;
        });

        $('\#jqGrid').jqGrid('setGridParam', {
            postData : jsonData
        }).trigger('reloadGrid');
    }
}
});

// 初始化
jQuery(function($) {
    // 加载jqgrid
    var editStr = $('\#jqGridEdit').html();
    $('\#jqGrid').jqGrid({
        url:dudu.ctx + "@{module}/@{crud.urlKey}/jqgrid",
        mtype: "POST",
        styleUI : 'Bootstrap',
        datatype: "json",
        colModel: [
            {label: "id",name: 'id',width: 75,hidden:true,key:true},
# for(entry in crud.listMap){ #
            {label: "@{entry.value.name}",name: '@{entry.value.javaKey}',width: 120,sortable:true},
# } #
            {
                name: '操作', index: '', width: 200, fixed: true, sortable: false, resize: false,
                formatter: function(cellvalue, options, rowObject) {
                    var replaceStr = "\\[id\\]";
                    var buttonStr = editStr;
                    try{
                        buttonStr = buttonStr.replace(/\r\n/g,"");
                        buttonStr = buttonStr.replace(/\n/g,"");
                        buttonStr = buttonStr.replace(new RegExp(replaceStr,'gm'),rowObject.id );
                    }catch(e) {
                        alert(e.message);
                    }
                    return buttonStr ;
                }
            }
        ],
        rownumbers: true,
        sortname: 'id',
        sortorder:'desc',
        viewrecords: true,
        autowidth: true,
        width: 1050,
        height: 630,
        rowNum: 20,
        caption: "@{crud.name}列表",
        pager: "\#jqGridPager"
    });

    // 宽高自适应
    $(window).resize(function(){
        $(window).unbind("onresize");
        $("\#jqGrid").setGridHeight($(window).height() - 160).jqGrid('setGridWidth', $('\#data_wrapper').width() - 5);
        $(window).bind("onresize", this);
    }).resize();

    $('\#jqGrid').jqGrid('navGrid',"\#jqGridPager", {
        search: false, // show search button on the toolbar
        add: false,
        edit: false,
        del: false,
        refresh: true,
        view: false
    });

});