package @{packagePath}.action;

import com.alibaba.fastjson.JSONObject;
import com.channelsoft.ccuas.common.controller.BaseController;
import com.channelsoft.ccuas.common.util.DateUtils;
import com.channelsoft.ccuas.system.po.SysUser;
import @{packagePath}.po.@{crud.table.className};
import com.channelsoft.ccuas.common.po.JSONData;
import @{packagePath}.service.I@{strutils.toUpperCaseFirst(crud.urlKey)}Service;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresUser;
import java.util.List;

/**
 * @{crud.table.remarks} 控制层
 *
 * @author flyfox 369191470@qq.com on @{now}.
 */
@RestController
@RequestMapping(value = "/admin/@{module}/@{crud.urlKey}")
public class @{strutils.toUpperCaseFirst(crud.urlKey)}Controller extends BaseController{

    @Autowired
    private I@{strutils.toUpperCaseFirst(crud.urlKey)}Service service;

    /**
     * 入口页面
     */
    @RequestMapping(value = "/index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("admin/@{module}/@{crud.urlKey}Index");
        return view;
    }

    /**
     * 获取单个@{crud.table.remarks}对象
     */
    @RequestMapping(value = "/get/{id}")
    @ResponseBody
    @RequiresUser
    public JSONData get(@PathVariable Integer id) {
        try {
            return success(service.getOne(id));
        }catch (Exception e){
            logger.error("获取@{crud.table.remarks}异常",e);
            return fail("获取@{crud.table.remarks}异常");
        }
    }


    /**
     * 获取@{crud.table.remarks}列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    @RequiresUser
    public JSONData list(PageInfo page
        , @RequestParam(required = false) @{crud.table.className} form) {
        try {
            List<@{crud.table.className}> list = service.getList(page, form);
            //slider为5
            PageInfo pageInfo = new PageInfo(list,5);
            JSONObject ret = new JSONObject();
            ret.put("data",list);
            ret.put("pageInfo",pageInfo);
            return success(ret);
        } catch (Exception e) {
            logger.error("获取@{crud.table.remarks}列表异常",e);
            return fail("获取@{crud.table.remarks}列表异常");
        }
    }

    /**
     * 添加@{crud.table.remarks}
     */
    @RequestMapping(value="/add")
    @ResponseBody
    @RequiresUser
    public JSONData add(@{crud.table.className} model){
        try {
            SysUser user = getUserInfo();
            model.setUpdateId(user.getUserId());
            model.setUpdateTime(DateUtils.getNow());
            model.setCreateId(user.getUserId());
            model.setCreateTime(DateUtils.getNow());
            service.add(model);
            return success(null, "添加@{crud.table.remarks}成功");
        } catch (Exception e) {
            logger.error("添加@{crud.table.remarks}列表异常",e);
            return fail("添加@{crud.table.remarks}列表异常");
        }
    }

    /**
     * 更新@{crud.table.remarks}
     */
    @RequestMapping(value="/update")
    @ResponseBody
    @RequiresUser
    public JSONData update(@{crud.table.className} model){
        try {
            SysUser user = getUserInfo();
            model.setUpdateId(user.getUserId());
            model.setUpdateTime(DateUtils.getNow());
            service.update(model);
            return success(null, "更新@{crud.table.remarks}成功");
        } catch (Exception e) {
            logger.error("更新@{crud.table.remarks}异常",e);
            return fail("更新@{crud.table.remarks}异常");
        }
    }

    /**
     * 删除@{crud.table.remarks}
     */
    @RequestMapping(value = "/delete/{id}")
    @ResponseBody
    @RequiresAuthentication
    public Object delete(@PathVariable Integer id) {
        try {
            if (service.delete(id) <= 0 ) {
                return fail("删除失败");
            }
            return success(null);
        } catch (Exception e) {
            logger.error("刪除@{crud.table.remarks}异常",e);
            return fail("刪除@{crud.table.remarks}异常");
        }
    }

}