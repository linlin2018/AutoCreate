package @{packagePath}.service;

import @{packagePath}.po.@{crud.table.className};
import com.channelsoft.ccuas.common.exception.ServiceException;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @{crud.table.remarks} 服务接口层
 *
 * @author flyfox 369191470@qq.com on @{now}.
 */
public interface I@{strutils.toUpperCaseFirst(crud.urlKey)}Service {

    /**
     * 获取@{crud.table.remarks}对象
     *
     * @Param id
     * @return
     * @throws ServiceException
     */
    public @{crud.table.className} getOne(Integer id) throws ServiceException;

    /**
     * 获取@{crud.table.remarks}对象列表
     * @return
     * @throws ServiceException
     */
    public List<@{crud.table.className}> getList(PageInfo page, @{crud.table.className} form) throws ServiceException;

    /**
     * 新增@{crud.table.remarks}信息
     *
     * @Param model
     * @return
     * @throws ServiceException
     */
    public int add(@{crud.table.className} model)throws ServiceException;

    /**
     * 更新@{crud.table.remarks}信息
     *
     * @Param model
     * @return
     * @throws ServiceException
     */
    public int update(@{crud.table.className} model)throws ServiceException;

    /**
     * 根据主键删除
     *
     * @Param id
     * @return
     * @throws ServiceException
     */
    public int delete(Integer id) throws ServiceException;

}