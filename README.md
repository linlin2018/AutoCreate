# AutoCreate

#### 项目介绍
AutoCreate 是 数据库链接采用Jfinal ActiveRecordPlugin，模板配置采用beetl，实现根据模板自动生成项目代码。
可以根据自己项目代码结构，定制属于自己的模板，甚至可以通过备注配置，实现select，radio，date等组件生成；模板配置完成，再也不用写那些重复的代码了。

#### 运行配置说明

1. 下载项目代码，安装jdk、maven、mysql（个人项目输出）。
2. 在项目目录下运行mvn install，提示BUILD SUCCESS即可。
3. java项目启动类：AutoCreateClientDemo；go项目启动类：AutoCreateGoClientDemo
4. 模板大家可以参考已有模板定制;
5. 已支持的项目列表
    1. jfinal_cms：https://gitee.com/jflyfox/jfinal_cms
    2. jflyfox: https://gitee.com/jflyfox/jflyfox
    3. gmanager: https://gitee.com/goflyfox/gmanager
    4. 其他未开源项目，仅做参考
6. 修改运行类的inti数据库链接参数，链接个人项目数据库地址。
7. 修改运行类配置
```
String module = "system"; // 模块名
String packagePath = "system"; // 包名
String tables = "agent_sys_user"; // 表名，支持逗号分割多个表,如tb_user,tb_organ
......
// 设置选择的模板
new AutoCreate().setTemplatePath("/autopath/template/project/gogf/").setPackagePath(packagePath).setModule(module).setCrudMap(crudMap).create();
```
8. 如果将项目打包，可以使用AutoCreateClient启动类，配置通过db.properties和template.properties修改，参考start.bat运行。