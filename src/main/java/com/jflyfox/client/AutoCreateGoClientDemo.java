package com.jflyfox.client;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jflyfox.autocreate.template.CRUD;
import com.jflyfox.autocreate.util.AutoCreate;
import com.jflyfox.autocreate.util.DbUtils;
import com.jflyfox.util.StrUtils;

import java.sql.SQLException;
import java.util.Map;

public class AutoCreateGoClientDemo {

    public static void main(String[] args) throws Exception {
        run();
    }


    protected static void run() throws SQLException, Exception {
        init();
        // 模块名
        String module = "system";
        // 包名
        String packagePath = "system";
        // 生成的表，支持逗号分割
        String tables = "sys_department,sys_user,sys_log,sys_menu,sys_role,sys_role_menu,sys_user_role,sys_config";
        // 模板路径
        String templatePath = "/autopath/template/project/gm/";

        Map<String, CRUD> crudMap = null;
        if (StrUtils.isEmpty(tables) || "all".equalsIgnoreCase(tables)) {
            crudMap = DbUtils.getCRUDMap();
        } else {
            String[] tableArray = tables.split(",");
            crudMap = DbUtils.getCRUDMap(tableArray);
        }

//		System.setProperty("user.dir","D:\\workspace\\project\\AutoCreate");
        new AutoCreate().setTemplatePath(templatePath).setPackagePath(packagePath).setModule(module).setCrudMap(crudMap).create();
    }

    public static void init() {

		String jdbcUrl = "jdbc:mysql://127.0.0.1:13306/gmanager?" +
                "serverTimezone=UTC&zeroDateTimeBehavior=convertToNull&useUnicode=true&characterEncoding=utf8";
		String user = "root";
		String password = "123456";
		String driverClass = "com.mysql.cj.jdbc.Driver";

        System.out.println("####jdbcUrlRead:" + jdbcUrl);
        System.out.println("####user:" + user);
        System.out.println("####password:" + password.trim());
        System.out.println("####driverClass:" + driverClass);

        C3p0Plugin c3p0Plugin = new C3p0Plugin(jdbcUrl, user, password.trim(), driverClass);

        // 配置ActiveRecord插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
        c3p0Plugin.start();
        arp.start();
    }

}
